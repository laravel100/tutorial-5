<?php

use App\Http\Controllers\StudentController;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');


Route::resource('students', StudentController::class);

Route::get('/redistest', function() {
    return Redis::set('a','b');
});

Route::group(['middleware' => 'auth'], function() {
    Route::get('/user-profile', [App\Http\Controllers\UserProfileController::class, 'index']);
    Route::post('/user-profile', [App\Http\Controllers\UserProfileController::class, 'store']);
});

require __DIR__.'/auth.php';
